const express = require('express')

const server = express();



const PORT = 3000;

server.use(express.static("webpack-angularjs/dist"));
server.use('/react', express.static('react-app/dist'));


server.listen(PORT, () => {
  console.log(`Server listening on http://127.0.0.1:${PORT}`)
});
