import React from 'react';

const App: React.FC = () => {
  const handleClick = () => {
    window.location.assign('/');
  };

  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <img width='200px' src='react/assets/images/logo.png' />
      <h1>React app</h1>
      <button onClick={handleClick}> go to angular!</button>
    </div> 
  );
};
export default App;
